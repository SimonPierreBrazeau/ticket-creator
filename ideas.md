# Ideas

The script should have setting files in a named folder. When the script runs,
it should recursively go up directories until it finds a directory containing
the config files. If it reaches the root directory, it should check the user's
home directory. If it can't find anything, it should use the default in the 
script's directory.

The config file should include things like project's name, where the tickets
should be written, where the issues' index file is.

The user should just need to call the scrpit and give it a string containing 
the name of the issue. The script will then generate a file with the issue
number first, then the issue's name. In the file itself this is what should be
generated:

```
= [Project name] - Issue [###] =
*Issue name*:     [Name of issue]
*Issue ID*:       [###]
*Date Opened*:    [Date generated]
*Date Closed*:    --
*Closing Commit*: --

*Description*:
```

If possible, the script should open the user's default editor to get a 
description to put in the file itself, so that he/she doesn't need to edit the
file manually after it has been generated.

Finally, once the ticket file has been generated and if the user added the 
issues's index file in the project's config file, the script should go in that
index file, look for an "open issues" section and go to the bottom of the list
to add a link to the file it just generated.


## Summoning Text Editor

_Under Windows_
`os.system([text file])`

_UNIX_
`os.system('%s %s' % (os.getenv('EDITOR'), filename))`


## Issue Listing File

This is what the issue listing file should look like:

```
= [Project bane] - Issues =

This is where the project's issue and feature tickets are located.

== Open Issues ==

== Closed Issues ==

```
