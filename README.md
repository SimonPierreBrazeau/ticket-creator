# Ticket Creator

## Description
This script creates issue ticket files using the narkdown format. Before 
generating a ticket file, the script looks for a configuration file in parent
directories until it either finds one or reaches the root directory without
finding any. Then, it asks for the name of the ticket. This string can be 
passed when calling the script. Next, the script will call the system's editor
for the user to write the description of the issue. Finally, the script creates
a ticket file, containing the creation date and ticket information. Also, if
the script can find an issue listing file, it will append a link to the ticket
file.

If the script can't find any configuration files, it will ask the user to 
create one in the present working directory. This file contains the project 
name and the relative path to a directory containing the ticket files.

If the script can't find an issue listing file, it will generate one.

## Usage
```python ticket-creator.py ["ticket name"]```
