'''
==== TICKET CREATOR ====
Author:  Simon-Pierre Brazeau
Date:    February 23, 2018
Version: 1.1

= Usage =
python ticket-creator.py ["ticket name"]

= Description =
This script creates issue ticket files using the markdown format. Before 
generating a ticket file, the script looks for a configuration file in parent
directories until it either finds one or reaches the root directory without
finding any. Then, it asks for the name of the ticket. This string can be 
passed when calling the script. Next, the script will call the system's editor
for the user to write the description of the issue. Finally, the script creates
a ticket file, containing the creation date and ticket information. Also, if
the script can find an issue listing file, it will append a link to the ticket
file.

If the script can't find any configuration files, it will ask the user to 
create one in the present working directory. This file contains the project 
name and the relative path to a directory containing the ticket files.

If the script can't find an issue listing file, it will generate one.
'''

### IMPORT LIBS ###
import sys
import os
import datetime
from os import listdir
from os.path import isfile, join

### MAIN CLASS ###
class TicketCreator:
    def __init__ (self):
        """
        Creates an instance of TicketCreator.
        """
        self.projectName = ""
        self.ticketLocation = ""
        self.issueListingFile = ""
        self.configRead = False
        self.CONFIG_FILE = ".ticketrc"
        self.configFileLocation = ""
        self.projectRoot = "./"

        self.CALLING_LOCATION = os.getcwd()
        self.ISSUES_FILE_NAME = "issues.md"
        self._configNameVar = "project_name:"
        self._configTicketVar = "ticket_location:"

        # Ticket file contents
        self._header = "# {0} - Issue {1}"
        self._issueName     = "**Issue Name**:     "
        self._issueNumber   = "**Issue ID**:       "
        self._dateOpened    = "**Date Opened**:    "
        self._dateClosed    = "**Date Closed**:    --"
        self._closingCommit = "**Closing Commit**: --"
        self._description   = "## Description"
        self._dateString = ""

        # Listing file contents
        self._listingHeader = "# {0} - Issues"
        self._listingDescription = "This is where the project's issue and feature tickets are located."
        self._listingOpenHeader = "## Open Issues"
        self._listingClosedHeader = "## Closed Issues"

        self._ticketNumber = 0
        self._ticketNumberStr = ""
        self._ticketFullName = ""
        self._ticketFile = ""
        self._ticket_full_path = ''
        self._ticketName = ""

        self._ticketDescription = ""
        self._ticketString = ""

        self._descFile = ".tmpDesc"


    def _GenerateConfigFile (self, projectName = None, ticketLocation = None):
        """
        Generates a configuration file in present working directory.
        
        Parameters
        ----------
        projectName : string
            Name of the project
        ticketLocation : string
            Relative path to directory containing ticket files
        """
        # Set required variables
        if projectName == None:
            self.projectName = input("Project name: ")
        else:
            self.projectName = projectName

        if ticketLocation == None:
            self.ticketLocation = input("Ticket location: ")
        else:
            self.ticketLocation = ticketLocation

        # Make sure the ticket location has a slash at the end of it
        if not self.ticketLocation.endswith("/"):
            self.ticketLocation += "/"

        # Write variables to config file
        finalString = ""
        finalString += self._configNameVar + self.projectName + "\n"
        finalString += self._configTicketVar + self.ticketLocation + "\n"

        finalFile = open(self.CONFIG_FILE, 'w+')
        finalFile.write(finalString)
        finalFile.close()
        
        self.configRead = True


    # Find the config file in parent directories
    def _FindConfigFile (self):
        """
        Explore parent directories to find a configuration file.

        If a configuration file is not found, it will ask the user if they want
        to create one. However, if the configuration file is located, it will
        read its parameters.
        """
        localDir = ""
        configFound = False
        while localDir != "/":
            localDir = os.getcwd()
            if os.path.isfile(self.CONFIG_FILE):
                configFound = True
                self.configFileLocation = os.getcwd() + "/" + self.CONFIG_FILE
                break
            
            os.chdir("../")

        if not configFound:
            os.chdir(self.CALLING_LOCATION)
            user_input = input("Config file was not located. Generate one? [y] ")
            if user_input != "" and user_input != "y" and user_input != "Y":
                print("Aborded.")
                return

            self._GenerateConfigFile()
            return

        # Read file
        data = None
        with open(self.configFileLocation, 'r') as file:
            data = file.readlines()

        # Check data for variables
        for line in data:
            if line.startswith(self._configNameVar):
                self.projectName = line.replace(self._configNameVar, '')
                self.projectName = self.projectName[:-1]
            elif line.startswith(self._configTicketVar):
                self.ticketLocation = line.replace(self._configTicketVar, '')
                self.ticketLocation = self.ticketLocation[:-1]

        if self.projectName == '' or self.ticketLocation == '':
            print("Missing variables in config file. Make sure all the variables are present or generate a new file.")
            return

        self.configRead = True


    def _GenerateIssueListingFile (self):
        """
        Generates an issue listing file and writes its contents to a file.
        """
        # Generate string for file
        finalString = self._listingHeader.format(self.projectName)
        finalString += "\n\n"
        finalString += self._listingDescription
        finalString += "\n\n"
        finalString += self._listingOpenHeader
        finalString += "\n\n"
        finalString += self._listingClosedHeader

        # Write generated string to file
        tf = open(self.issueListingFile, "w")
        tf.write(finalString)
        tf.close()


    def _CheckFilesAndDir (self):
        """
        Verify that the required files and directory are present.

        If any of them are not present, this method creates them.
        """
        # Make sure the issue listing path is set
        if self.issueListingFile == "":
            self.issueListingFile = self.ticketLocation + self.ISSUES_FILE_NAME

        # Check if ticket directory exists
        if not os.path.isdir(self.ticketLocation):
            # Create directory
            print("Ticket directory doesn't exist. Creating directory.")
            os.makedirs(self.ticketLocation)

        # Check if issue listing file exists
        if not os.path.isfile(self.issueListingFile):
            print("Issue listing file doesn't exist. Creating one.")
            self._GenerateIssueListingFile()


    def _AppendLinkToIssues (self):
        """
        Creates a link to the generated ticket file in the issue listing file.
        """
        data = None     # Issue listing file's data

        # Read issue listing file and save data in memory
        with open(self.issueListingFile, 'r') as file:
            data = file.readlines()

        # Go through lines and find opening header
        openingLine = -1
        for l in data:
            openingLine += 1
            if l == self._listingOpenHeader + "\n":
                break

        # Make sure we found opening header
        if openingLine >= len(data):
            print("Invalid listing file format. Not adding ticket link.")
            return

        # Find closing header
        closingLine = -1
        for l in data:
            closingLine += 1
            if l == self._listingClosedHeader + "\n":
                break

        # Make sure we found the closing header and that it's at the right place
        if closingLine >= len(data) or closingLine < openingLine:
            print("Invalid listing file format. Not adding ticket link.")
            return

        # Look for last open ticket
        lastItemIndex = closingLine
        while lastItemIndex > openingLine:
            lastItemIndex -= 1
            if data[lastItemIndex] != "\n":
                lastItemIndex += 1
                break

        # Make sure last item index is not opening line
        if lastItemIndex == openingLine:
            lastItemIndex += 1

        # Add link to data array
        linkString = "[{0}](./{0})  \n".format(self._ticketFullName)
        data.insert(lastItemIndex, linkString)

        # Write new data to file
        finalFile = open(self.issueListingFile, 'w')
        for l in data:
            finalFile.write(l)
        finalFile.close()


    def _GenerateIssueNumber (self):
        """
        Looks inside the ticket directory for the large ticket number, and sets
        the current ticket number one above it.
        """
        # Get a list of all the files in ticket location
        listFiles = [f for f in listdir(self.ticketLocation) if isfile(join(self.ticketLocation, f))]

        # Value of biggest ticket number in files
        topTicket = 0

        # Go through the files and look for the highest number
        for f in listFiles:
            fch = f[:3]
            
            # Are first 3 characters digits?
            if fch.isdigit():
                fnum = int(fch)

                # Check if chars are bigger than largest ticket
                if fnum > topTicket:
                    topTicket = fnum

        # Set ticket number to 1 above top ticket gathered
        self._ticketNumber = topTicket + 1
        self._ticketNumberStr = str(self._ticketNumber).rjust(3, '0')


    def _GenerateOpeningDate (self):
        """
        Creates a string containing the current date.
        """
        dt = datetime.date.today()
        self._dateString += str(dt.day).rjust(2, '0') + "/" 
        self._dateString += str(dt.month).rjust(2, '0') + "/" 
        self._dateString += str(dt.year)[-2:]


    def _GetDescription (self):
        """
        Calls the operating system's editor for the user to write the issue's
        description in a temporary file, then grab that description.
        """
        # Call OS' editor
        os.system('%s %s' % (os.getenv('EDITOR'), self._descFile))

        # Grab description from file
        df = open(self._descFile, 'r')
        lines = df.readlines()

        # Append every line of the file to description string
        for l in lines:
            self._ticketDescription += l

        df.close()

        # Delete descrpition file
        os.remove(self._descFile)


    def _GenerateTicketString (self):
        """
        Creates a string with the ticket's content.
        """
        ts = ""
        ts += self._header.format(self.projectName, self._ticketNumberStr) + "\n"
        ts += self._issueName + self._ticketName + "  \n"
        ts += self._issueNumber + self._ticketNumberStr + "  \n"
        ts += self._dateOpened + self._dateString + "  \n"
        ts += self._dateClosed + "  \n"
        #ts += self._closingCommit + "  \n"
        ts += "\n"
        ts += self._description + "\n"
        ts += self._ticketDescription

        self._ticketString = ts


    def _WriteToFile (self):
        """
        Writes the ticket's contents to its file.
        """
        tf = open(self._ticket_full_path, "w")
        tf.write(self._ticketString)
        tf.close()


    def GenerateTicket (self, issueName = None, launchEditor = True, issueNumber = -1):
        """
        Creates an issue ticket and appends a link to the issue listing file.
        
        Parameters
        ----------
        issueName : string
            Name of the issue
        launchEditor : boolean
            Launch OS's editor for user to write description
        issueNumber : int
            ID number of the ticket
        """
        # Check if required files are present
        self._FindConfigFile()
        
        if self.configRead:
            self._CheckFilesAndDir()

            # Make sure ticket name is given
            if issueName == None:
                self._ticketName = input("Ticket name: ")
            else:
                self._ticketName = issueName

            print("Generating ticket for {0}...".format(self._ticketName))

            # Generate issue number if necessary
            if issueNumber == -1:
                # Check files to generate a ticket number
                self._GenerateIssueNumber()
            else:
                self._ticketNumber = issueNumber

            # Create a string for name of ticket file
            self._ticketFullName = self._ticketNumberStr + " - " + self._ticketName
            self._ticketFile = self._ticketFullName + ".md"
            self._ticket_full_path = self.ticketLocation + self._ticketFile

            # Get description from user
            if launchEditor:
                self._GetDescription()

            # Generate opening date string
            self._GenerateOpeningDate()

            # Generate ticket string
            self._GenerateTicketString()

            # Create the file
            self._WriteToFile()

            # Add link to issue listing file
            self._AppendLinkToIssues()

### SCRIPT CONTENTS ###
if __name__ == "__main__":
    tc = TicketCreator()

    # Check if ticket name was given
    if len(sys.argv) > 1:
        tc.GenerateTicket(sys.argv[1])
    else:
        tc.GenerateTicket()
